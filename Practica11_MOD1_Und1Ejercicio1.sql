﻿USE ciclistas;

/** Consulta 1 Listar las edades de todos
  los ciclistas de Banesto **/

    SELECT DISTINCT 
      c.edad
    FROM ciclista c
    WHERE
    c.nomequipo='Banesto';


/** consulta 2 Listar las edades de los ciclistas que son de Banesto 
  o de Navigare **/

  SELECT DISTINCT 
  c.edad 
  FROM ciclista c
  WHERE
  c.nomequipo='Banesto'
  OR
  c.nomequipo='Navigare';

SELECT DISTINCT
  c.edad
  FROM
  ciclista c
  WHERE
c.nomequipo IN ('Banesto', 'Navigare');



/** consulta 3 Listar el dorsal de los ciclistas que son 
  de Banesto y cuya edad esta entre 25 y 32 **/
  
SELECT c.dorsal
 FROM ciclista c
WHERE
c.nomequipo='Banesto'AND c.edad BETWEEN 25 AND 32;


/**consulta 4 Listar el dorsal de los ciclistas
  que son de Banesto o cuya edad esta entre 25
  y 32  **/

  SELECT c.dorsal
     FROM ciclista c
  WHERE c.nomequipo='Banesto'
    OR c.edad
  BETWEEN 25 AND 32;

/**consulta 5 Listar la inicial del equipo 
  de los ciclistas cuyo nombre comience por R
  **/

SELECT DISTINCT LEFT(c.nomequipo,1) 
FROM ciclista c
WHERE
c.nomequipo LIKE'R%';

SELECT DISTINCT LEFT(c.nomequipo,1)
 FROM ciclista c
 WHERE 
   LEFT (c.nomequipo,1)='R';

SELECT DISTINCT LEFT(c.nomequipo,1) 
FROM ciclista c
WHERE
  UPPER(LEFT(c.nombre,1))='R';


/** Consulta 6 Listar el código de las etapas que
  su salida y llegada sea en la misma población.
  **/

  SELECT e.numetapa 
  FROM 
  etapa e
  WHERE
  e.salida=e.llegada;


/** Consulta 7 Listar el código de las etapas 
  que su salida y llegada no sean en la misma 
  población y que conozcamos el dorsal del 
  ciclista que ha ganado la etapa **/

  SELECT e.numetapa 
  FROM etapa e
  WHERE
  e.salida<>e.llegada AND e.dorsal IS NOT NULL;

/** Consulta 8 Listar el nombre de los puertos
  cuya altura entre 1000 y 2000 o que la altura
  sea mayor que 2400. **/

SELECT p.nompuerto 
FROM puerto p
WHERE
p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;

/** Consulta 9 Listar el dorsal de los ciclistas
que hayan ganado los puertos cuya altura entre 
1000 y 2000 o que la altura sea mayor que 2400. **/

SELECT DISTINCT p.dorsal
 FROM puerto p
WHERE
p.altura BETWEEN 1000 AND 2000 OR p.altura>2400;



/** Consulta 10 Listar el número de ciclistas 
  que hayan ganado alguna etapa **/

SELECT COUNT(DISTINCT dorsal) AS nCiclistas  
FROM etapa e;

SELECT COUNT(*) AS nCiclistas  
FROM (SELECT DISTINCT e.dorsal FROM etapa e) AS c1;


/** Consulta 11 Listar el número de etapas que 
  tengan puerto **/

SELECT COUNT(DISTINCT p.numetapa) AS nEtapas 
FROM puerto p;

/** 12 Listar el número de ciclistas que hayan 
  ganado algún puerto **/

  SELECT COUNT(DISTINCT p.dorsal) AS nCiclistas
  FROM puerto p;


/**13 Listar el código de la etapa con el número
  de puertos que tiene**/

  SELECT numetapa,COUNT(*) AS nPuertos 
  FROM puerto p
  GROUP BY
   numetapa;


/**Consulta 14 Indicar la altura media de los puertos
  **/

SELECT AVG(p.altura) 
FROM puerto p;


/** Consulta 15 Indicar el código de etapa cuya altura media de sus puertos
  está por encima de 1500. **/

SELECT numetapa 
 FROM puerto p
GROUP BY
numetapa
HAVING
AVG(p.altura)>1500;

SELECT 
 p.numetapa,
AVG(altura) AS aMedia
 FROM puerto p
GROUP BY
p.numetapa;



SELECT p.numetapa
 FROM (SELECT p.numetapa, AVG(altura) AS aMedia FROM puerto p GROUP BY p.numetapa) AS c1
 WHERE 
  c1.aMedia>1500;


/** consulta 16 Indicar el número de etapas que cumplen la
  condición anterior. **/

  SELECT p.numetapa
  FROM puerto p
  GROUP BY
  p.numetapa
  HAVING
  AVG(p.altura)>1500;

  
  SELECT COUNT(*) AS nEtapas FROM(SELECT p.numetapa FROM puerto p
  GROUP BY p.numetapa
  HAVING AVG(p.altura)>1500) AS C1;


/** consulta 17 Listar el dorsal del ciclista con el número
  de veces que ha llevado algún maillot **/

SELECT l.dorsal, COUNT(*) numero FROM lleva l
GROUP BY l.dorsal;



/** Consulta 18  Listar el dorsal del ciclista con el código de maillot y cuantas veces ese ciclista ha
llevado ese maillot. **/


SELECT l.dorsal, l.código, COUNT(*) AS numero
 FROM lleva l
GROUP BY
l.dorsal, l.código;



 /** consulta 19 Listar el dorsal el código de etapa, el ciclista y el número de maillots que ese ciclista a
llevado en cada etapa. **/

  SELECT c1.*, c.nombre FROM( SELECT
  l.dorsal, l.numetapa, COUNT(*) AS numero
     FROM lleva l
    GROUP BY l.dorsal, l.numetapa) AS c1
    INNER JOIN
    ciclista c
    ON c1.dorsal=c.dorsal;


  SELECT c.dorsal,c.nombre, l.numetapa, COUNT(*) AS numero
  FROM lleva l
    JOIN
  ciclista c
  ON l.dorsal = c.dorsal
  GROUP BY
  c.dorsal, l.numetapa
  ;
